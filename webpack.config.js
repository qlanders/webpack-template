// TODO [gostev] 09.07.2018 fix sourcemaps if it posible

const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ZipPlugin = require('zip-webpack-plugin');

const PATHS = {
    source: path.join(__dirname, 'src'),
    dist: path.join(__dirname, 'dist')
};

const dirName = path.basename(__dirname);

const correctNumber = number => (number < 10 ? "0" + number : number);
const GetTimestamp = () => {
    const now = new Date();
    const year = now.getFullYear();
    const month = correctNumber(now.getMonth() + 1);
    const day = correctNumber(now.getDate());
    const hours = correctNumber(now.getHours());
    const minutes = correctNumber(now.getMinutes());

    return `${year}-${month}-${day}-${hours}${minutes}`;
};

const cssDev = [
    'style-loader',
    {
        loader: 'css-loader',
        options: {
            sourceMap: true
        }
    },
    'postcss-loader',
    'stylus-loader'
];

const cssProd = [
    {
        loader: MiniCssExtractPlugin.loader,
        options: {
            publicPath: '../'
        }
    },
    'css-loader',
    'postcss-loader',
    'stylus-loader'
];


module.exports = (env, options) => {
    const inProduction = options.mode === 'production';

    return {
        devtool: inProduction ? false : 'cheap-module-source-map',
        entry: {
            index: PATHS.source + '/static/js/index.js'
        },
        output: {
            path: PATHS.dist,
            filename: "js/[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: "babel-loader",
                            options: {
                                presets: ["env"]
                            }
                        }
                    ]
                },
                {
                    test: /\.pug$/,
                    use: 'pug-loader'
                },
                {
                    test: /\.styl$/,
                    use: inProduction ? cssProd : cssDev
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192,
                                name: 'images/[name].[ext]',
                                fallback: 'file-loader'
                            }
                        },
                        {
                            loader: 'image-webpack-loader',
                            options: {
                                bypassOnDebug: true,
                                mozjpeg: {
                                    progressive: true,
                                    quality: 65
                                },
                                optipng: {
                                    enabled: false
                                },
                                pngquant: {
                                    quality: '65-90',
                                    speed: 4
                                },
                                gifsicle: {
                                    interlaced: false
                                }
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 50000,
                                name: 'fonts/[name].[ext]',
                                fallback: 'file-loader'
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin([
                './dist'
            ]),
            new HtmlWebpackPlugin({
                template: PATHS.source + '/pages/index.pug',
                filename: 'index.html',
                hash: true
            }),
            new HtmlWebpackPlugin({
                template: PATHS.source + '/pages/new.pug',
                filename: 'pages/new.html',
                hash: true
            }),
            new MiniCssExtractPlugin({
                filename: "css/styles.css"
            }),
            new webpack.HotModuleReplacementPlugin(),
            new ZipPlugin({
                filename: `${dirName}-${GetTimestamp()}.zip`
            })
        ],
        devServer: {
            contentBase: PATHS.dist,
            hot: true
        }
    }
};
